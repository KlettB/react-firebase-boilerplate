This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting started

The easiest way to get started is to clone the repository:

```bash
# Get the project
git clone git@gitlab.com:KlettB/react-firebase-boilerplate.git myproject

# Change directory
cd myproject

# Install dependencies
yarn install
```

Create your firebase project (https://firebase.google.com/)<br>
Create a new .env file in the root of your project and add your firebase configs

```conf
REACT_APP_API_KEY="your-firebase-api-key"
REACT_APP_AUTH_DOMAIN="your-domain.firebaseapp.com"
REACT_APP_DATABASE_URL="https://your-domain.firebaseio.com"
REACT_APP_PROJECT_ID="react-firebase-boilderplate"
REACT_APP_STORAGE_BUCKET=""
REACT_APP_MESSAGING_SENDER_ID="your-messaging-sender-id"
```

Activate email/password as authentication method and create a new user via the firebase console ui.
https://firebase.google.com/docs/auth/


Run your application

```bash
# Then simply start your app
yarn start
```