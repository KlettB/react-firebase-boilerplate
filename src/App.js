import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { withAuthentication } from './components/session';
import Navigation from './components/navigation/navigation';
import DashboardPage from './pages/dashboard/dashboard';
import LoginPage from './pages/login/login';

import './App.css';

const App = () => (
  <Router>
    <React.Fragment>
      <Navigation />

      <Route exact path="/" component={DashboardPage} />
      <Route path="/login" component={LoginPage} />
    </React.Fragment>
  </Router>
);

export default withAuthentication(App);
