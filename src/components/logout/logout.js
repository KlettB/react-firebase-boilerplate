import React from 'react';

import { withFirebase } from '../firebase';

const LogoutButton = ({ firebase }) => (
  <button className="btn btn-sm btn-primary" onClick={firebase.signOut}>Logout</button>
);

export default withFirebase(LogoutButton);
