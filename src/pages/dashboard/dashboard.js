import React from 'react';

import { AuthUserContext, withAuthorization } from '../../components/session';

const DashboardPage = () => (
    <AuthUserContext.Consumer>
        {authUser => (
            <div className="container mt-4">
                <div className="row">
                    <div className="col-12">
                        <h1 className="display-4">Dashboard</h1>
                        <p>Eingeloggt als {authUser.email}</p>
                    </div>
                </div>
            </div>
        )}
    </AuthUserContext.Consumer>
);

const condition = authUser => !!authUser;

export default withAuthorization(condition)(DashboardPage);