import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import { withFirebase } from '../../components/firebase';

const LoginPage = () => (
    <div id="loginPage" className="d-flex justify-content-center align-items-center text-center">
        <div className="login-form">
            <h1 className="display-4 mb-5">Login</h1>
            <LoginForm />
        </div>
    </div>
);

const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
};

class LoginFormBase extends Component {
    constructor(props) {
        super(props);

        this.state = { ...INITIAL_STATE };
    }

    onSubmit = event => {
        const { email, password } = this.state;

        this.props.firebase
            .signInWithEmailAndPassword(email, password)
            .then(authUser => {
                console.log('login successful');
                this.setState({ ...INITIAL_STATE });
                this.props.history.push('/dashboard');
            })
            .catch(error => {
                console.log('login error');
                this.setState({ error });
            });

        event.preventDefault();
    };

    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        const {
            email,
            password,
            error,
        } = this.state;

        const isInvalid = password === '' || email === '';

        return (
            <form onSubmit={this.onSubmit}>
                <div class="form-group">
                    <input
                        name="email"
                        value={email}
                        onChange={this.onChange}
                        type="email"
                        placeholder="Email Address"
                        className="form-control"
                    />
                </div>
                <div class="form-group">
                    <input
                        name="password"
                        value={password}
                        onChange={this.onChange}
                        type="password"
                        placeholder="Password"
                        className="form-control"
                    />
                </div>


                <button disabled={isInvalid} type="submit" className="btn btn-primary btn-block">Login</button>

                {error && <p>{error.message}</p>}
            </form>
        );
    }
}

const LoginForm = compose(
    withRouter,
    withFirebase,
)(LoginFormBase);

export default LoginPage

export { LoginForm };

